use strict;
use warnings;

use Cwd qw(cwd);
use POSIX qw(uname);
use Test::More;
use Test::Command::Simple;

my $arch = (POSIX::uname)[4];
my $basepath = Cwd::cwd();
my $testlog_path = $ENV{AUTOPKGTEST_ARTIFACTS}.'/netatalk_spectest.txt';
my $params = '-7 -C -v -h 127.0.0.1 -p 548 -u atalk1 -d atalk2 -w afpafp -s test1 -S test2';

if ($arch eq 's390x') {
  $params = '-X ' . $params;
}

run('sudo systemctl stop netatalk');
run('sudo groupadd -f afpusers');
run('sudo useradd -G afpusers atalk1');
run('sudo useradd -G afpusers atalk2');
run('echo "atalk1:afpafp" | sudo chpasswd');
run('echo "atalk2:afpafp" | sudo chpasswd');
run('sudo mkdir -p /tmp/afptest1');
run('sudo mkdir -p /tmp/afptest2');
run('sudo chown atalk1:afpusers /tmp/afptest1');
run('sudo chown atalk1:afpusers /tmp/afptest2');
run('sudo chmod 2775 /tmp/afptest1');
run('sudo chmod 2775 /tmp/afptest2');
run('sudo cp '.$basepath.'/debian/tests/afp.conf /etc/netatalk/');
run('sudo cp '.$basepath.'/debian/tests/extmap.conf /etc/netatalk/');
run('sudo systemctl start netatalk');
sleep(2);
run_ok('afp_spectest ' . $params);

my $testlog = stdout;
open(TESTLOG, ">$testlog_path") or die "$0: open $testlog_path: $!";
print(TESTLOG $testlog);

done_testing;

